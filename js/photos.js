let imgNumber = 1;
function update() {
    document.querySelector('#image').src = "img/" + imgNumber + ".JPG";
    document.querySelector('#counter').textContent = imgNumber + " / 54";
}
document.querySelector('#next').onclick = function (e) {
    if (imgNumber < 54) {
        imgNumber++;
    }
    update();
}
document.querySelector('#prev').onclick = function (e) {
    if (imgNumber > 1) {
        imgNumber--;
    }
    update();
}
update();
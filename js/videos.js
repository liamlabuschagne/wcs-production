let videos = [
    "https://www.youtube-nocookie.com/embed/JA-zHfV0C_A",
    "https://www.youtube-nocookie.com/embed/3Y11TfohSVg",
    "https://www.youtube-nocookie.com/embed/9hgf0cosPpw",
];

let videoNumber = 0;

function update() {
    document.querySelector('#video-box iframe').src = videos[videoNumber];
    document.querySelector('#counter').textContent = (videoNumber + 1) + " / 3";
}
document.querySelector('#next').onclick = function (e) {
    if (videoNumber < 3) {
        videoNumber++;
    }
    update();
}
document.querySelector('#prev').onclick = function (e) {
    if (videoNumber > 0) {
        videoNumber--;
    }
    update();
}
update();